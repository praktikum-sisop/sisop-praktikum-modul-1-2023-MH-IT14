#!/bin/bash

regions=("Mondstat" "Liyue" "Inazuma" "Sumeru" "Fontaine")

# Function to process a region
process_region() {
  local region="$1"
  cd "/home/nicholas/genshin_character/$region" || return

  for file in *; do
    if [ -f "$file" ]; then
      process_file "$file"
    fi

    if [ "$status" = true ]; then
      break
    fi

    sleep 60
  done

  cd - > /dev/null || return
}

# Function to process a file
process_file() {
  local file="$1"
  time=$(date +"%Y/%m/%d %H:%M:%S")
  image_path="./$file"

  steghide extract -sf "$file" -p ""
  name=$(awk -F' -' '{print $1}' <<< "$file")
  cat "$name.txt" | base64 -d > ./retracted.txt

  status=false
  url=$(cat ./retracted.txt)
  url_valid='https?://\S+'

  if [[ $url =~ $url_valid ]]; then
    wget "$url"
    status=true
  fi

  echo "[$time] [$status] [$image_path]" >> ./image.log

  if [ "$status" = true ]; then
    return
  fi

  rm "$name".txt
}

# Main loop
for region in "${regions[@]}"; do
  process_region "$region"

  if [ "$status" = true ]; then
    break
  fi
done
