#!/bin/bash
mkdir genshin_character
wget 'https://drive.google.com/uc?export=download&id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2' -O genshin.zip 
unzip genshin.zip -d genshin_character

cd genshin_character

unzip "genshin_character.zip"


# decode base64
  for encoded_file in *.jpg; do
    new_name=$(echo "$encoded_file" | base64 -d)
    mv "$encoded_file" "$new_name".jpg
    echo "Renamed $encoded_file to $new_name"
  done  
echo "Berhasil rename."

# looping list_character.csv
while IFS=, read -r Nama Region Element Senjata; do
    # rename
    new_filename="$(echo "${Nama} - ${Region} - ${Element} - ${Senjata}.jpg" | tr -d '\r')"

    # cari .jpg
    for file in *.jpg; do
        # Check if the current file matches the character name
        if [[ "$file" =~ ^$Nama\.jpg$ ]]; then
            # Rename the file
            mv "$file" "$new_filename"
            echo "Renamed $file to $new_filename"
            break  # Exit the loop once renamed
        fi
    done
done < list_character.csv

# Create folders for each region
mkdir -p Mondstat Inazuma Sumeru Liyue Fontaine

# Loop through each .jpg file
for file in *.jpg; do
    # Extract the character's region from the file name
    region=$(echo "$file" | awk -F ' - ' '{print $2}')
    
    # Check if the region folder exists
    if [ -d "$region" ]; then
        # Move the file to the corresponding region folder
        mv "$file" "$region/"
        echo "Moved $file to $region/"
    else
        echo "Folder $region does not exist. Skipping $file"
    fi
done

echo "Claymore = $(find /home/nicholas/genshin_character -type f -name "*.jpg" | grep -o 'Claymore' | wc -l)"
echo "Sword = $(find /home/nicholas/genshin_character -type f -name "*.jpg" | grep -o 'Sword' | wc -l)"
echo "Bow = $(find /home/nicholas/genshin_character -type f -name "*.jpg" | grep -o 'Bow' | wc -l)"
echo "Catalyst = $(find /home/nicholas/genshin_character -type f -name "*.jpg" | grep -o 'Catalyst' | wc -l)"
echo "Polearm = $(find /home/nicholas/genshin_character -type f -name "*.jpg" | grep -o 'Polearm' | wc -l)"

rm genshin_character.zip list_character.csv; cd ..; rm genshin.zip



