#!/bin/bash

validate_login(){
 local email="$1"
 local password="$2"
 local username=""
 local stored_password=""

 # check username
 if ! grep -q "^$email" users.txt; then
  echo "[LOGIN FAILED ] - $email not registed, please register first"
  return 1
 fi

 username=$(grep "^$email" users.txt | cut -d ' ' -f 2)
 stored_password=$(grep "^$email" users.txt | cut -d ' ' -f 3)

 # Decrypt the stored password
 decrypted_password=$(echo "$stored_password" | base64 -d)

 # check password
 if [ "$password" != "$decrypted_password" ]; then
   echo "LOGIN FAILED - Incorrect password for user $username"
   return 1
 fi

 echo "[$(date '+%d/%m/%y %H:%M:%S')] [LOGIN SUCCESS] user $username login successfully" >> auth.log
 echo "LOGIN SUCCESS - Welcome, $username"
 return 0
}

# checkig
if [[ $# -ne 2 ]]; then
  echo "Usage: ./login.sh [email] [password]"
  exit 1
fi

email="$1"
password="$2"

validate_login "$email" "$password"
