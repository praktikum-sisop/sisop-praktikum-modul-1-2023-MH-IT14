#!/bin/bash

encrypt_password(){
 echo -n "$1" | base64
}

validate_password(){
 local password="$1"
 local username="$2"

 if [[ ${#password} -lt 8 ]]; then
   echo "Password harus 8 karakter"
   return 1
 fi

 if ! [[ "$password" =~ [A-Z] ]]; then
   echo "Password harus memiliki huruf kapital"
   return 1
 fi

 if ! [[ "$password" =~ [a-z] ]]; then
   echo "Password harus memiliki huruf kecil"
   return 1
 fi

 if ! [[ "$password" =~ [!@#$%^*()] ]]; then
 echo "Password harus memiliki karakter special"
 return 1
 fi

 if [[ "$password" == "$username" ]]; then
 echo "Password Tidak boleh sama dengan username"
 return 1
 fi

 return 0
}

register(){
 local email="$1"
 local username="$2"
 local password="$3"

 # no. a
 if grep -q "^$email" users.txt; then 
 echo "$email is already registred."
 return 1
 fi

 # no. b
 if  ! validate_password "$password" "$username"; then 
 return 1
 fi

 local encrypted_password=$(encrypt_password "$password")

 #no. c
 echo "$email $username $encrypted_password" >> users.txt

 #no. d & g
 echo "[$(date '+%d/%m/%y %H:%M:%S')] [REGISTER SUCCESS] user $username registered successfully" >> auth.log" 
}

 if [[ $# -ne 3 ]]; then
  echo "Usage": ./register.sh [email] [username] [password]
  exit 1
 fi

email="$1"
username="$2"
password="$3"

register "$email" "$username" "$password"
