#!/bin/bash

# Cron per jam
# 0 * * * * ./aggregate_minutes_to_hourly_log.sh

# Buat hasil filenya
log_directory="./logs"
timestamp=$(date +"%Y%m%d%H")
file="$log_directory/metrics_agg_${timestamp}.log"

# definisikan path
username=$(whoami)
target_path="/home/$username/"

# Ambil minute.log file
minute_log_files=($log_directory/metrics_${timestamp}*.log)

# Deklarasi variabel untuk aggregasi
declare -i mem_total_min=999999 mem_total_max=0
declare -i mem_used_min=999999 mem_used_max=0
declare -i mem_free_min=999999 mem_free_max=0
declare -i mem_shared_min=999999 mem_shared_max=0
declare -i mem_buff_min=999999 mem_buff_max=0
declare -i mem_available_min=999999 mem_available_max=0
declare -i swap_total_min=999999 swap_total_max=0
declare -i swap_used_min=999999 swap_used_max=0
declare -i swap_free_min=999999 swap_free_max=0
declare -i total_path_size=0 max_path_size=0 min_path_size=999999999

declare -i total_mem_total=0 total_mem_used=0 total_mem_free=0 total_mem_shared=0 total_mem_buff=0 total_mem_available=0
declare -i total_swap_total=0 total_swap_used=0 total_swap_free=0
declare -a path_sizes=() total_path_size=0

# Inisialisasi variabel
for minute_log_file in "${minute_log_files[@]}"; do
    while IFS=, read -r mem_total_column mem_used_column mem_free_column mem_shared_column mem_buff_column mem_available_column swap_total_column swap_used_column swap_free_column path_column path_size_column; do
        # Ambil data numerik dari kolom-kolom
        declare -i mem_total mem_used mem_free mem_shared mem_buff mem_available swap_total swap_used swap_free path_size
        
        path_size="${path_size_column//[^0-9]/}"  # Remove non-numeric characters
        if [[ -n "$path_size" ]]; then
            path_size="$((path_size * 1))"  # Convert to numeric value
        else
            path_size=0  # Set to 0 if it's not a valid number
        fi

        mem_total="$mem_total_column"
        mem_used="$mem_used_column"
        mem_free="$mem_free_column"
        mem_shared="$mem_shared_column"
        mem_buff="$mem_buff_column"
        mem_available="$mem_available_column"
        swap_total="$swap_total_column"
        swap_used="$swap_used_column"
        swap_free="$swap_free_column"

        # Aggregasikan minimum dan maksimum metrik
        if ((mem_total < mem_total_min && mem_total > 0)); then
            mem_total_min="$mem_total"
        fi
        if ((mem_total > mem_total_max)); then
            mem_total_max="$mem_total"
        fi

        if ((mem_used < mem_used_min && mem_used > 0)); then
            mem_used_min="$mem_used"
        fi
        if ((mem_used > mem_used_max)); then
            mem_used_max="$mem_used"
        fi

        if ((mem_free < mem_free_min && mem_free > 0)); then
            mem_free_min="$mem_free"
        fi
        if ((mem_free > mem_free_max)); then
            mem_free_max="$mem_free"
        fi

        if ((mem_shared < mem_shared_min && mem_shared > 0)); then
            mem_shared_min="$mem_shared"
        fi
        if ((mem_shared > mem_shared_max)); then
            mem_shared_max="$mem_shared"
        fi

        if ((mem_buff < mem_buff_min && mem_buff > 0)); then
            mem_buff_min="$mem_buff"
        fi
        if ((mem_buff > mem_buff_max)); then
            mem_buff_max="$mem_buff"
        fi

        if ((mem_available < mem_available_min && mem_available > 0)); then
            mem_available_min="$mem_available"
        fi
        if ((mem_available > mem_available_max)); then
            mem_available_max="$mem_available"
        fi

        if ((swap_total < swap_total_min && swap_total > 0)); then
            swap_total_min="$swap_total"
        fi
        if ((swap_total > swap_total_max)); then
            swap_total_max="$swap_total"
        fi

        if ((swap_used < swap_used_min)); then
    	swap_used_min="$swap_used"
	fi

	if ((swap_used > swap_used_max)); then
    	swap_used_max="$swap_used"
	fi

	if ((swap_free < swap_free_min && swap_free > 0)); then
    	swap_free_min="$swap_free"
	fi

	if ((swap_free > swap_free_max)); then
    	swap_free_max="$swap_free"
	fi
	if ((path_size < min_path_size && path_size > 0)); then
            min_path_size="$path_size"
        fi
        if ((path_size > max_path_size)); then
        max_path_size="$path_size"
    	fi

        # Simpan path size untuk diaggregasikan
        path_sizes+=("$path_size")

        # Aggregasikan total metrik
        total_mem_total="$((total_mem_total + mem_total))"
        total_mem_used="$((total_mem_used + mem_used))"
        total_mem_free="$((total_mem_free + mem_free))"
        total_mem_shared="$((total_mem_shared + mem_shared))"
        total_mem_buff="$((total_mem_buff + mem_buff))"
        total_mem_available="$((total_mem_available + mem_available))"
        total_swap_total="$((total_swap_total + swap_total))"
        total_swap_used="$((total_swap_used + swap_used))"
        total_swap_free="$((total_swap_free + swap_free))"
        total_path_size="$((total_path_size + path_size))"
    done < "$minute_log_file"
done

# Hitung rata-rata metrik
mem_total_avg="$((total_mem_total / 3))"
mem_used_avg="$((total_mem_used / 3))"
mem_free_avg="$((total_mem_free / 3))"
mem_shared_avg="$((total_mem_shared / 3))"
mem_buff_avg="$((total_mem_buff / 3))"
mem_available_avg="$((total_mem_available / 3))"
swap_total_avg="$((total_swap_total / 3))"
swap_used_avg="$((total_swap_used / 3))"
swap_free_avg="$((total_swap_free / 3))"
average_path_size="$((total_path_size / 3))M"

# Simpan hasil aggregasinya
echo "type, mem_total, mem_used, mem_free, mem_shared, mem_buff, mem_available, swap_total, swap_used, swap_free, path_size" > "$file"
echo "minimum:, $mem_total_min, $mem_used_min, $mem_free_min, $mem_shared_min, $mem_buff_min, $mem_available_min, $swap_total_min, $swap_used_min, $swap_free_min, $target_path, ${min_path_size}M" >> "$file"
echo "maximum:, $mem_total_max, $mem_used_max, $mem_free_max, $mem_shared_max, $mem_buff_max, $mem_available_max, $swap_total_max, $swap_used_max, $swap_free_max, $target_path, ${max_path_size}M" >> "$file"
echo "average:, $mem_total_avg, $mem_used_avg, $mem_free_avg, $mem_shared_avg, $mem_buff_avg, $mem_available_avg, $swap_total_avg, $swap_used_avg, $swap_free_avg, $target_path, $average_path_size" >> "$file"

# d. Membuat semua .log hanya dapat dibaca oleh owner
chmod 600 $log_directory/*.log
