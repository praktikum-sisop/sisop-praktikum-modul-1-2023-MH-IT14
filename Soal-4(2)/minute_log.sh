#!/bin/bash

#Note semua log dimasukan ke direktory log
log_directory="./logs"
mkdir -p "$log_directory"

#variable untuk monitoring ram $2-7 = mem_total mem_used mem_free mem_buff mem_avalable 0 = swab_total, swab used, swab free mengunakan awk 
timestamp=$(date +"%Y%m%d%H%M%S")

# Monitor RAM usage and store the results in the ram_info variable
ram_info=$(free -m | grep -E 'Mem' | awk '{printf "%s, %s, %s, %s, %s, %s", $2, $3, $4, $5, $6, $7}')
swap_info=$(free -m | grep -E 'Swap' | awk '{printf "%s, %s, %s", $2, $3, $4}')

#variable untuk monitoring directory size ke /home/{user}, user disini saya menggunakan variable agar tidak terpaku ke user saya sendiri
username=$(whoami)
target_path="/home/$username/"
directory_size=$(du -sh "$target_path" | cut -f1)

#Untuk menyimpan semua variable menjadi 1 line dan dimasukkan ke metrics
echo "mem_total, mem_used, mem_free, mem_shared, mem_buff, mem_available, swap_total, swap_used, swap_free, path, path_size" > "$log_directory/metrics_${timestamp}.log"
metrics="$ram_info, $swap_info, $target_path, $directory_size"
echo "$metrics" >> "$log_directory/metrics_${timestamp}.log"

#b. cron every minute (min hour day bulan minggu)
# * * * * * ./minute_log.sh