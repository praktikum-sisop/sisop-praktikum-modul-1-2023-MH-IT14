# Laporan
### IT14
| NRP | Nama |
| ------ | ------ |
| 5027221001 | Dwiyasa Nakula |
| 5027221042 |Nicholas Marco Weinandra |
- [Soal 1](https://gitlab.com/praktikum-sisop/sisop-praktikum-modul-1-2023-MH-IT14/-/blob/main/README.md#nomor-1)
- [Soal 2](https://gitlab.com/praktikum-sisop/sisop-praktikum-modul-1-2023-MH-IT14/-/blob/main/README.md#nomer-2)
- [Soal 3](https://gitlab.com/praktikum-sisop/sisop-praktikum-modul-1-2023-MH-IT14/-/blob/main/README.md#nomer-3)
- [Soal 4](https://gitlab.com/praktikum-sisop/sisop-praktikum-modul-1-2023-MH-IT14/-/blob/main/README.md#nomer-4)

# Nomor 1
<details><summary>Click to expand</summary>
Farrel ingin membuat sebuah playlist yang sangat edgy di tahun ini. Farrel ingin playlistnya banyak disukai oleh orang-orang lain. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa data untuk membuat playlist terbaik di dunia. Untung saja Farrel menemukan file playlist.csv yang berisi top lagu pada spotify beserta genrenya.

a) Farrel ingin memasukan lagu yang bagus dengan genre hip hop. Oleh karena itu, Farrel perlu melakukan testi terlebih dahulu. Tampilkan 5 top teratas lagu dengan genre hip hop berdasarkan popularity.

b) Karena Farrel merasa kurang dengan lagu tadi, dia ingin mencari 5 lagu yang paling rendah diantara lagu ciptaan John Mayer berdasarkan popularity.

c) Karena Farrel takut lagunya kurang enak didengar dia ingin mencari lagu lagi, cari 10 lagu pada tahun 2004 dengan rank popularity tertinggi

d) Farrel sangat suka dengan lagu paling keren dan edgy di dunia. Lagu tersebut diciptakan oleh ibu Sri. Bantu Farrel mencari lagu tersebut dengan kata kunci nama pencipta lagu tersebut. 
</details>

## Pengerjaan Soal Nomor 1

#### Jawaban 1a :

```bash
#!/bin/bash
wget --no-check-certificate -r 'https://drive.google.com/uc?export=download&id=1JYkupCAZTmwPZSZRsB61cklPM-LUjRHp' -O top_lagu_milik_farrel.csv

grep 'hip hop' top_lagu_milik_farrel.csv | awk -F',' 'NR > 1 {print $0 | "sort -t, -k15,15nr"}' | head -n 5
```
#### Pembahasan 1a :
Pertama, kita harus men-*download* file playlist.csv yang ada di GDrive menggunakan command **wget**. Selanjutnya, command yang dipakai untuk menampilkan top 5 lagu genre hip-hop berdasarkan popularity nya adalah **grep**, **awk**, **sort**, dan **head**.
<br>
- Perintah **grep** digunakan untuk mencari kata "hip hop" dalam file CSV 'top_lagu_milik_farrel.csv'.
- **-F','** digunakan untuk mengatur delimiter (pemisah) untuk awk menjadi tanda koma (,), karena pada file .csv menggunakan koma sebagai pemisah antar kolom.
- **NR > 1** digunakan untuk menjalankan perintah selanjutnya pada setiap baris kecuali baris pertama.
- **{print $0 | "sort -t, -k15,15nr"}** digunakan untuk mengambil seluruh baris ($0) dan mengirimkannya ke perintah **sort** dengan opsi berikut:
    - **-t,** digunakan untuk mengatur tanda koma sebagai  delimiter untuk sort.
    - **k15,15nr** digunakan untuk mengatakan kepada sort untuk mengurutkan berdasarkan kolom ke-15 dalam urutan numerik menurun (nr).
- **head -n 5** digunakan untuk menampilkan 5 baris pertama dari hasil output.

#### Output 1a :
![output 1a](images/Screenshot_2023-09-29_010552.png)

#### Jawaban 1b :

```bash
grep 'John Mayer' top_lagu_milik_farrel.csv | awk -F',' 'NR > 1 {print $0 | "sort -t, -k15,15n"}' | head -n 5
```
#### Pembahasan 1b :
Pengerjaan soal 1b kurang lebih sama dengan nomor soal 1a. Perbedaannya hanyalah penggunaan **grep** untuk mencari kata kunci 'John Mayer' dan penggunaan **k15,15nr** ntuk mengurutkan berdasarkan kolom ke-15 dalam urutan numerik meningkat.

#### Output 1b :

![output 1b](images/Screenshot_2023-09-29_020002.png)

#### Jawaban 1c :

```bash
grep '2004' top_lagu_milik_farrel.csv | awk -F',' 'NR > 1 {print $0 | "sort -t, -k15,15nr"}' | head -n 10
```
#### Pembahasan 1c :
Pengerjaan soal 1c kurang lebih sama dengan nomor soal 1a dan 1b. Perbedaannya adalah penggunaan **grep** untuk mencari kata kunci '2004', penggunaan **k15,15nr** ntuk mengurutkan berdasarkan kolom ke-15 dalam urutan numerik menurun, dan penggunaan **head -n 10** digunakan untuk menampilkan 10 baris pertama dari hasil output.

#### Output 1c :

![output 1b](images/Screenshot_2023-09-29_020632.png)

#### Jawaban 1d :

```bash
grep 'Sri' top_lagu_milik_farrel.csv
```
#### Pembahasan 1d :
Pengerjaan soal 1d dilakukan dengan menggunakan command **grep** untuk mencari kata kunci 'Sri'

#### Output 1d :
![output 1b](images/Screenshot_2023-09-29_021011.png)

# Nomer 2
#### Soal
<details><summary>Click to expand</summary>

Shinichi merupakan seorang detektif SMA yang kembali menjadi anak kecil karena ulah organisasi hitam. Dengan tubuhnya yang mengecil, Shinichi tidak dapat menggunakan identitas lamanya sehingga harus membuat identitas baru. Selain itu, ia juga harus membuat akun baru dengan identitas nya saat ini. Bantu Shinichi *membuat program register dan login* agar Shinichi dapat dengan mudah membuat semua akun baru yang ia butuhkan.

 a. Shinichi akan melakukan *register menggunakan email, username, serta password. Username yang dibuat bebas, namun email bersifat unique.*

 b. Shinichi khawatir suatu saat nanti Ran akan curiga padanya dan dapat mengetahui password yang ia buat. Maka dari itu, Shinichi ingin membuat password dengan tingkat keamanan yang tinggi. 
Password tersebut harus di encrypt menggunakan 
- base64
- Password yang dibuat *harus lebih dari 8 karakter*
- Harus terdapat *paling sedikit 1 huruf kapital dan 1 huruf kecil*
- Password *tidak boleh sama dengan username*
- Harus terdapat *paling sedikit 1 angka* 
- Harus terdapat *paling sedikit 1 simbol unik*

 c. Karena Shinichi akan membuat banyak akun baru, ia berniat untuk *menyimpan seluruh data register yang ia lakukan ke dalam folder users file users.txt. Di dalam file tersebut, terdapat catatan seluruh email, username, dan password* yang telah ia buat.

 d. Shinichi juga ingin *program register yang ia buat akan memunculkan respon setiap kali ia melakukan register.* Respon ini akan menampilkan apakah register yang dilakukan Shinichi berhasil atau gagal

 e. Setelah melakukan register, Shinichi akan langsung melakukan login untuk memastikan bahwa ia telah berhasil membuat akun baru. *Login hanya perlu dilakukan menggunakan email dan password.*

 f. Ketika *login berhasil ataupun gagal, program akan memunculkan respon* di mana respon tersebut harus mengandung username dari email yang telah didaftarkan.

> Ex: 
> LOGIN SUCCESS - Welcome, [username]
> LOGIN FAILED - email [email] not registered, please register first

g. Shinichi juga *mencatat seluruh log ke dalam folder users file auth.log*, baik login ataupun register.

> Format: [date] [type] [message]
> 
> Type: REGISTER SUCCESS, REGISTER FAILED, LOGIN SUCCESS, LOGIN FAILED
> 
> Ex:
> [23/09/17 13:18:02] [REGISTER SUCCESS] user [username] registered successfully
> [23/09/17 13:22:41] [LOGIN FAILED] ERROR Failed login attempt on user with email [email]
</details>

## Pengerjaan Soal No. 2

##### register.sh

```
#!/bin/bash

#no. b
encrypt_password(){ 
 echo -n "$1" | base64
}

validate_password(){
 local password="$1"
 local username="$2"

 if [[ ${#password} -lt 8 ]]; then
   echo "Password harus 8 karakter"
   return 1
 fi

 if ! [[ "$password" =~ [A-Z] ]]; then
   echo "Password harus memiliki huruf kapital"
   return 1
 fi

 if ! [[ "$password" =~ [a-z] ]]; then
   echo "Password harus memiliki huruf kecil"
   return 1
 fi

 if ! [[ "$password" =~ [!@#$%^*()] ]]; then
 echo "Password harus memiliki karakter special"
 return 1
 fi

 if [[ "$password" == "$username" ]]; then
 echo "Password Tidak boleh sama dengan username"
 return 1
 fi

 return 0
}

register(){
 local email="$1"
 local username="$2"
 local password="$3"

 # no. a
 if grep -q "^$email" users.txt; then 
 echo "$email is already registred."
 return 1
 fi

 # no. b
 if  ! validate_password "$password" "$username"; then 
 return 1
 fi

 local encrypted_password=$(encrypt_password "$password")

 #no. c
 echo "$email $username $encrypted_password" >> users.txt

 #no. d & g
 echo "[REGISTER SUCCESS] user $username registered successfully"
 echo "[$(date '+%d/%m/%y %H:%M:%S')] [REGISTER SUCCESS] user $username registered successfully" >> auth.log" 
}

 if [[ $# -ne 3 ]]; then
  echo "Usage": ./register.sh [email] [username] [password]
  exit 1
 fi

email="$1"
username="$2"
password="$3"

register "$email" "$username" "$password"
```

##### login.sh
```
#!/bin/bash

#function untuk validasi login
validate_login(){
 local email="$1"
 local password="$2"
 local username=""
 local stored_password=""

 #check username apakah sudah ada sebelumnya di users.txt
 if ! grep -q "^$email" users.txt; then
  echo "[LOGIN FAILED ] - $email not registed, please register first"
  return 1
 fi

 #mengambil email dan password dari users.txt
 username=$(grep "^$email" users.txt | cut -d ' ' -f 2)
 stored_password=$(grep "^$email" users.txt | cut -d ' ' -f 3)

 # Decrypt passwordnya menggunakan base64
 decrypted_password=$(echo "$stored_password" | base64 -d)

 # melakukan pengecekan pada password yang diinput dengan yang disimpan
 if [ "$password" != "$decrypted_password" ]; then
   echo "LOGIN FAILED - Incorrect password for user $username"
   return 1
 fi

 #respons
 echo "[$(date '+%d/%m/%y %H:%M:%S')] [LOGIN SUCCESS] user $username login successfully" >> auth.log
 echo "LOGIN SUCCESS - Welcome, $username"
 return 0
}

# control input command jika kurang dari input yang diperlukan
if [[ $# -ne 2 ]]; then
  echo "./login.sh [email] [password]"
  exit 1
fi

email="$1"
password="$2"

validate_login "$email" "$password"
```

#### Pembahasan Jawaban
##### a.

 ```
# no. a
 if grep -q "^$email" users.txt; then 
 echo "$email is already registred."
 return 1
 fi
```
Melakukan pengecekan pada email yang di register apakah sudah ada menggunakan `if` dan `grep` untuk mengambil email lalu di bandingkan dengan email yang sudah ada di `users.txt`, jika sama maka keluar $email is already registred dimana `$email` adalah email yang diinputkan tadi. tidak ada kendala pada pengerjaanmya.

##### b.
```
# no. b
if  ! validate_password "$password" "$username"; then 
 return 1
fi

local encrypted_password=$(encrypt_password "$password")

encrypt_password(){
 echo -n "$1" | base64
}

validate_password(){
 local password="$1"
 local username="$2"

 if [[ ${#password} -lt 8 ]]; then
   echo "Password harus 8 karakter"
   return 1
 fi

 if ! [[ "$password" =~ [A-Z] ]]; then
   echo "Password harus memiliki huruf kapital"
   return 1
 fi

 if ! [[ "$password" =~ [a-z] ]]; then
   echo "Password harus memiliki huruf kecil"
   return 1
 fi

 if ! [[ "$password" =~ [!@#$%^*()] ]]; then
 echo "Password harus memiliki karakter special"
 return 1
 fi

 if [[ "$password" == "$username" ]]; then
 echo "Password Tidak boleh sama dengan username"
 return 1
 fi

 return 0
}
```

Kode ini akan mengambil password dan username dan akan di cek apakah sudah sesuai dengan kriteria. pengecekan akan terjadi di fungsi `validate_password(){` menggunakan `if` dan untuk mengenkripsi dengan `base64` akan menggunakan `encrypt_password(){` dengan cara kita ambil passwordnya `echo -n "$1"` lalu kita akan encripsi pakai base64 `| base64`. tidak ada kendala dalam mengerjakan.


##### c.
```
 #no. c
echo "$email $username $encrypted_password" >> users.txt
```
Pada bagian ini kita masukan semua variable yang sudah diambil tadi dan password nya juga sudah di enkripsi dan masukan ke `users.txt`
tidak ada masalah dalam pengerjaannya.

##### d & f & g.
```
#no. d & g
echo [REGISTER SUCCESS] user $username registered successfully"
echo "[$(date '+%d/%m/%y %H:%M:%S')] [REGISTER SUCCESS]   user $username registered successfully" >> auth.log"
}

#no. f & g
 echo "[$(date '+%d/%m/%y %H:%M:%S')] [LOGIN SUCCESS] user $username login successfully" >> auth.log
 echo "LOGIN SUCCESS - Welcome, $username"
 return 0
}
```
Kode ini akan memberitahu apakah kita sudah sukses atau belum dan begitu pula memasukan ke dalam `auth.log`. tidak ada masalah dalam mengerjakannya.

##### e.
```
email="$1"
password="$2"

validate_login "$email" "$password"
```

pada bagian ini akan membaca apa yang kita inputkan dan memasukannya ke dalam `validate_login` jika pada login.sh tidak ada yang gagal maka dia akan sukses dalam login. tidak ada masalah yang terjadi saat mengerjakan soal ini.

#### Output script
##### register.sh
![output register.sh](images/register.sh.png)
##### register.sh
![output login.sh](images/login.sh.png)
##### users.txt
![output users.txt](images/users.txt.png)
##### auth.log
![output auth.log](images/auth.log.png)

# Nomer 3

<details><summary>Click to expand</summary>
Aether adalah seorang gamer yang sangat menyukai bermain game Genshin Impact. Karena hobinya, dia ingin mengoleksi foto-foto karakter Genshin Impact. Suatu saat Pierro memberikannya sebuah Tautan yang berisi koleksi kumpulan foto karakter dan sebuah clue yang mengarah ke endgame. Ternyata setiap nama file telah dienkripsi dengan menggunakan base64. Karena penasaran dengan apa yang dikatakan piero, Aether tidak menyerah dan mencoba untuk mengembalikan nama file tersebut kembali seperti semula.

a. Aether membuat script bernama genshin.sh, untuk melakukan unzip terhadap file yang telah diunduh dan decode setiap nama file yang terenkripsi dengan base64 . Karena pada file list_character.csv terdapat data lengkap karakter, Aether ingin merename setiap file berdasarkan file tersebut. Agar semakin rapi, Aether mengumpulkan setiap file ke dalam folder berdasarkan region tiap karakter
Format: Nama - Region - Elemen - Senjata.jpg

b. Karena tidak mengetahui jumlah pengguna dari tiap senjata yang ada di folder "genshin_character".Aether berniat untuk menghitung serta menampilkan jumlah pengguna untuk setiap senjata yang ada
Format: [Nama Senjata] : [total]
	 Untuk menghemat penyimpanan. Aether menghapus file - file yang tidak ia gunakan, yaitu genshin_character.zip, list_character.csv, dan genshin.zip

c.Namun sampai titik ini Aether masih belum menemukan clue endgame yang disinggung oleh Pierro. Dia berfikir keras untuk menemukan pesan tersembunyi tersebut. Aether membuat script baru bernama find_me.sh untuk melakukan pengecekan terhadap setiap file tiap 1 detik. Pengecekan dilakukan dengan cara meng-ekstrak tiap gambar dengan menggunakan command steghide. Dalam setiap gambar tersebut, terdapat sebuah file txt yang berisi string. Aether kemudian mulai melakukan dekripsi dengan base64 pada tiap file txt dan mendapatkan sebuah url. Setelah mendapatkan url yang ia cari, Aether akan langsung menghentikan program find_me.sh serta mendownload file berdasarkan url yang didapatkan.

d. Dalam prosesnya, setiap kali Aether melakukan ekstraksi dan ternyata hasil ekstraksi bukan yang ia inginkan, maka ia akan langsung menghapus file txt tersebut. Namun, jika itu merupakan file txt yang dicari, maka ia akan menyimpan hasil dekripsi-nya bukan hasil ekstraksi. Selain itu juga, Aether melakukan pencatatan log pada file image.log untuk setiap pengecekan gambar
Format: [date] [type] [image_path]
Ex: 
[23/09/11 17:57:51] [NOT FOUND] [image_path]
[23/09/11 17:57:52] [FOUND] [image_path]

e.Hasil akhir:
**genshin_character
find_me.sh
genshin.sh
image.log
[filename].txt
[image].jpg**
</details>

## Pengerjaan Soal Nomor 3

#### Jawaban 3a dan 3b :
**genshin.sh**
```bash
#!/bin/bash
mkdir genshin_character
wget 'https://drive.google.com/uc?export=download&id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2' -O genshin.zip 
unzip genshin.zip -d genshin_character

cd genshin_character

unzip "genshin_character.zip"

# decode base64
  for encoded_file in *.jpg; do
    new_name=$(echo "$encoded_file" | base64 -d)
    mv "$encoded_file" "$new_name".jpg
    echo "Renamed $encoded_file to $new_name"
  done  
echo "Berhasil rename."

# looping list_character.csv
while IFS=, read -r Nama Region Element Senjata; do
    # rename
    new_filename="$(echo "${Nama} - ${Region} - ${Element} - ${Senjata}.jpg" | tr -d '\r')"

    # cari .jpg
    for file in *.jpg; do
        # Check if the current file matches the character name
        if [[ "$file" =~ ^$Nama\.jpg$ ]]; then
            # Rename the file
            mv "$file" "$new_filename"
            echo "Renamed $file to $new_filename"
            break  # Exit the loop once renamed
        fi
    done
done < list_character.csv

# Create folders for each region
mkdir -p Mondstat Inazuma Sumeru Liyue Fontaine

# Loop through each .jpg file
for file in *.jpg; do
    # Extract the character's region from the file name
    region=$(echo "$file" | awk -F ' - ' '{print $2}')
    
    # Check if the region folder exists
    if [ -d "$region" ]; then
        # Move the file to the corresponding region folder
        mv "$file" "$region/"
        echo "Moved $file to $region/"
    else
        echo "Folder $region does not exist. Skipping $file"
    fi
done

echo "Claymore = $(find /home/nicholas/genshin_character -type f -name "*.jpg" | grep -o 'Claymore' | wc -l)"
echo "Sword = $(find /home/nicholas/genshin_character -type f -name "*.jpg" | grep -o 'Sword' | wc -l)"
echo "Bow = $(find /home/nicholas/genshin_character -type f -name "*.jpg" | grep -o 'Bow' | wc -l)"
echo "Catalyst = $(find /home/nicholas/genshin_character -type f -name "*.jpg" | grep -o 'Catalyst' | wc -l)"
echo "Polearm = $(find /home/nicholas/genshin_character -type f -name "*.jpg" | grep -o 'Polearm' | wc -l)"

rm genshin_character.zip list_character.csv; cd ..; rm genshin.zip
```
#### Penjelasan 3a dan 3b (REVISI) :
- Langkah pertama adalah untuk membuat directory baru bernama **genshin_character** kemudian mengunduh genshin.zip yang ada di Google Drive menggunakan command **wget**.
- Langkah kedua, dilakukan **unzip** file genshin_character.zip
- Langkah ketiga, dilakukan decode nama dari semua file .jpg yang telah ter-enkripsi base64. Caranya adalah dengan melakukan for loop terhadap semua file .jpg lalu menggunakan command **base64 -d** untuk mendecode. Kemudian hasil decode teks tersebut akan digunakan untuk me-rename.
- Langkah keempat adalah untuk mencocokkan nama karakter pada nama file dengan file _list_character.csv_. Setelah itu, kita perlu untuk membaca file.csv tersebut dan menggunakan command **IFS=,** untuk memberi tahu bahwa separatornya adalah comma. Kemudian, dilakukan rename lagi sesuai format yang ditentukan yaitu Nama - Region - Elemen - Senjata.jpg
- Langkah kelima, dilakukan pengelompokkan file sesuai dengan folder region nya. Pertama, dibuat terlebih dahulu folder-folder region nya. Kemudian dilakukan looping di semua file .jpg untuk mengelompokkan nama file yang memiliki region yang sama. Setelah dicari, maka file-file tersebut akan dipindahkan ke folder region-nya masing-masing.
- Langkah keenam adalah mencari jumlah pengguna untuk setiap senjata yang ada menggunakan command : <br>
**echo "Claymore = $(find /home/nicholas/genshin_character -type f -name "*.jpg" | grep -o 'Claymore' | wc -l)"**
  -**echo Claymore** digunakan untuk mem-print teks "Claymore"
  -**find /home/nicholas/genshin_character** adalah direktori tempat pencarian dimulai
  -**-type f** adalah Parameter yang menunjukkan bahwa kita hanya ingin mencari file
  -**-name "*.jpg"** adalah kriteria pencarian. Perintah ini mencari file yang memiliki ekstensi .jpg.
  -**grep -o 'Claymore'** adalah perintah grep digunakan untuk mencari kata "Claymore" dalam hasil pencarian dari find.
  -**wc -l** merupakan perintah wc yang digunakan untuk menghitung kata, baris, dan karakter dalam teks yang diberikan.
<br>
-Langkah terakhir adalah untuk menghapus file genshin_character.zip, list_character.csv, dan genshin.zip menggunakan command **rm**.

#### Output 3a dan 3b
![output Folder](images/output_folder.png)
![output File](images/output_file.png)
![output Senjata](images/output_senjata.png)

#### Jawaban 3c dan 3d :
**find_me.sh**<br>
```bash
#!/bin/bash

regions=("Mondstat" "Liyue" "Inazuma" "Sumeru" "Fontaine")

# process region
process_region() {
  local region="$1"
  cd "/home/nicholas/genshin_character/$region" || return

  for file in *; do
    if [ -f "$file" ]; then
      process_file "$file"
    fi

    if [ "$status" = true ]; then
      break
    fi

    sleep 60
  done

  cd - > /dev/null || return
}

# Function to process a file
process_file() {
  local file="$1"
  time=$(date +"%Y/%m/%d %H:%M:%S")
  image_path="./$file"

  steghide extract -sf "$file" -p ""
  name=$(awk -F' -' '{print $1}' <<< "$file")
  cat "$name.txt" | base64 -d > ./retracted.txt

  status=false
  url=$(cat ./retracted.txt)
  url_valid='https?://\S+'

  if [[ $url =~ $url_valid ]]; then
    wget "$url"
    status=true
  fi

  echo "[$time] [$status] [$image_path]" >> ./image.log

  if [ "$status" = true ]; then
    return
  fi

  rm "$name".txt
}

# Main loop
for region in "${regions[@]}"; do
  process_region "$region"

  if [ "$status" = true ]; then
    break
  fi
done
```
#### Penjelasan 3c dan 3d :
-Langkah pertama adalah untuk mendeklarasikan semua folder region yang ada.
-**process_region()** merupakan fungsi yang digunakan untuk memproses setiap wilayah. Fungsi ini menerima parameter berupa nama wilayah, dan kemudian berpindah ke direktori yang sesuai dengan wilayah tersebut. Di dalamnya, fungsi akan membaca file-file yang ada di folder tersebut.
-process_file() berfungsi untuk memproses setiap file dalam wilayah yang sedang diproses. Fungsi ini menerima parameter berupa nama file. Di dalamnya, terdapat langkah-langkah untuk mengekstrak informasi dari file, seperti ekstraksi data tersembunyi, URL, dan lainnya.
-Pada akhirnya, skrip akan menghasilkan log yang mencatat waktu, status (berhasil atau tidak), dan path gambar yang sedang diproses ke dalam file ./image.log
#### Output 3c dan 3d :
![output 3cd](images/output_3cd.png)


# Nomer 4
<details><summary>Click to expand</summary>

Defatra sangat tergila-gila dengan laptop legion nya. Suatu hari, laptop legion nya mendadak rusak 🙁 Tentu saja, Defatra adalah programer yang harus 24/7 ngoding tanpa menggunakan vscode di laptop legion nya.  Akhirnya, dia membawa laptop legion nya ke tukang servis untuk diperbaiki. Setelah selesai diperbaiki, ternyata biaya perbaikan sangat mahal sehingga dia harus menggunakan uang hasil slot nya untuk membayarnya. Menurut Kang Servis, masalahnya adalah pada laptop Defatra yang overload sehingga mengakibatkan crash pada laptop legion nya. Untuk mencegah masalah serupa di masa depan, Defatra meminta kamu untuk membuat program monitoring resource yang tersedia pada komputer.

**Buatlah program monitoring resource** pada laptop kalian. Cukup **monitoring ram dan monitoring size suatu directory**. Untuk **ram gunakan command `free -m`.** Untuk **disk gunakan command `du -sh <target_path>`.** **Catat semua metrics yang didapatkan dari hasil `free -m`.** **Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk `target_path` yang akan dimonitor adalah `/home/{user}/`.**

a. **Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log.** {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2023-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20230131150000.log. 

b. **Script untuk mencatat metrics** diatas diharapkan dapat **berjalan otomatis pada setiap menit.** 

c. Kemudian, buat satu **script untuk membuat agregasi file log ke satuan jam.** **Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit.** Dalam hasil file agregasi tersebut, terdapat **nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics.** File agregasi akan ditrigger untuk **dijalankan setiap jam secara otomatis.** Berikut contoh nama file hasil agregasi metrics_agg_2023013115.log **dengan format metrics_agg_{YmdH}.log**

d. Karena file log bersifat sensitif pastikan **semua file log hanya dapat dibaca oleh user pemilik file.**

> Note:
> - Nama file untuk script per menit adalah minute_log.sh
> - Nama file untuk script agregasi per jam adalah aggregate_minutes_to_hourly_log.sh
> - Semua file log terletak di /home/{user}/log
> - Semua konfigurasi cron dapat ditaruh di file skrip .sh nya masing-masing dalam bentuk comment

> **Berikut adalah contoh isi dari file metrics yang dijalankan tiap menit:**
> mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size 15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M

> **Berikut adalah contoh isi dari file aggregasi yang dijalankan tiap jam:**
> type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size minimum,15949,10067,223,588,5339,4626,2047,43,1995,/home/user/test/,50M maximum,15949,10387,308,622,5573,4974,2047,52,2004,/home/user/test/,74M average,15949,10227,265.5,605,5456,4800,2047,47.5,1999.5,/home/user/test/,62M
</details>

## Pengerjaan Soal 4
##### minute_log.sh
```
#!/bin/bash

#Note semua log dimasukan ke direktory log
directory_log="./log"
mkdir -p "$directory_log"

#a. membuat variable timestamp untuk nama file dengan format date Year, bulan, hari, jam, menit, detik
timestamp=$(date +"%Y%m%d%H%M%S")

# Monitor RAM usage and store the results in the ram_info variable
ram_info=$(free -m | grep -E 'Mem' | awk '{printf "%s, %s, %s, %s, %s, %s", $2, $3, $4, $5, $6, $7}')

# Monitor swap usage and store the results in the swap_info variable
swap_info=$(free -m | grep -E 'Swap' | awk '{printf "%s, %s, %s", $2, $3, $4}')

#variable untuk monitoring directory size ke /home/{user}, user disini saya menggunakan variable agar tidak terpaku ke user saya sendiri
username=$(whoami)
target_path="/home/$username/"
directory_size=$(du -sh "$target_path" | cut -f1)

#Untuk menyimpan semua variable menjadi 1 line 
echo "mem_total, mem_used, mem_free, mem_shared, mem_buff, mem_available, swap_total, swap_used, swap_free, path,path_size" > "./logs/metrics_${timestamp}.log"
metrics="$ram_info, $target_path, $directory_size"
echo "$metrics" >> "./logs/metrics_${timestamp}.log"

#b. cron every minute (min hour day bulan minggu)
# * * * * * ./minute_log.sh
```

##### aggregate_minutes_to_hourly_log.sh

```
#!/bin/bash

# Cron per jam
# 0 * * * * ./aggregate_minutes_to_hourly_log.sh

# Buat hasil filenya
log_directory="./logs"
timestamp=$(date +"%Y%m%d%H")
file="$log_directory/metrics_agg_${timestamp}.log"

# Ambil minute.log file
minute_log_files=($log_directory/metrics_${timestamp}*.log)

# Deklarasi variabel untuk aggregasi
declare -i mem_total_min=999999 mem_total_max=0
declare -i mem_used_min=999999 mem_used_max=0
declare -i mem_free_min=999999 mem_free_max=0
declare -i mem_shared_min=999999 mem_shared_max=0
declare -i mem_buff_min=999999 mem_buff_max=0
declare -i mem_available_min=999999 mem_available_max=0
declare -i swap_total_min=999999 swap_total_max=0
declare -i swap_used_min=999999 swap_used_max=0
declare -i swap_free_min=999999 swap_free_max=0
declare -i total_path_size=0 max_path_size=0 min_path_size=999999999

declare -i total_mem_total=0 total_mem_used=0 total_mem_free=0 total_mem_shared=0 total_mem_buff=0 total_mem_available=0
declare -i total_swap_total=0 total_swap_used=0 total_swap_free=0
declare -a path_sizes=()

# Inisialisasi variabel
for minute_log_file in "${minute_log_files[@]}"; do
    while IFS=, read -r mem_total_column mem_used_column mem_free_column mem_shared_column mem_buff_column mem_available_column swap_total_column swap_used_column swap_free_column _ _ path_size_column; do
        # Ambil data numerik dari kolom-kolom
        declare -i mem_total mem_used mem_free mem_shared mem_buff mem_available swap_total swap_used swap_free path_size

        mem_total="$mem_total_column"
        mem_used="$mem_used_column"
        mem_free="$mem_free_column"
        mem_shared="$mem_shared_column"
        mem_buff="$mem_buff_column"
        mem_available="$mem_available_column"
        swap_total="$swap_total_column"
        swap_used="$swap_used_column"
        swap_free="$swap_free_column"
        path_size="$path_size_column"

        # Aggregasikan minimum dan maksimum metrik
        if ((mem_total < mem_total_min && mem_total > 0)); then
            mem_total_min="$mem_total"
        fi
        if ((mem_total > mem_total_max)); then
            mem_total_max="$mem_total"
        fi

        if ((mem_used < mem_used_min && mem_used > 0)); then
            mem_used_min="$mem_used"
        fi
        if ((mem_used > mem_used_max)); then
            mem_used_max="$mem_used"
        fi

        if ((mem_free < mem_free_min && mem_free > 0)); then
            mem_free_min="$mem_free"
        fi
        if ((mem_free > mem_free_max)); then
            mem_free_max="$mem_free"
        fi

        if ((mem_shared < mem_shared_min && mem_shared > 0)); then
            mem_shared_min="$mem_shared"
        fi
        if ((mem_shared > mem_shared_max)); then
            mem_shared_max="$mem_shared"
        fi

        if ((mem_buff < mem_buff_min && mem_buff > 0)); then
            mem_buff_min="$mem_buff"
        fi
        if ((mem_buff > mem_buff_max)); then
            mem_buff_max="$mem_buff"
        fi

        if ((mem_available < mem_available_min && mem_available > 0)); then
            mem_available_min="$mem_available"
        fi
        if ((mem_available > mem_available_max)); then
            mem_available_max="$mem_available"
        fi

        if ((swap_total < swap_total_min && swap_total > 0)); then
            swap_total_min="$swap_total"
        fi
        if ((swap_total > swap_total_max)); then
            swap_total_max="$swap_total"
        fi

        if ((swap_used < swap_used_min)); then
    	swap_used_min="$swap_used"
	fi

	if ((swap_used > swap_used_max)); then
    	swap_used_max="$swap_used"
	fi

	if ((swap_free < swap_free_min && swap_free > 0)); then
    	swap_free_min="$swap_free"
	fi

	if ((swap_free > swap_free_max)); then
    	swap_free_max="$swap_free"
	fi

        # Simpan path size untuk diaggregasikan
        path_sizes+=("$path_size")

        # Aggregasikan total metrik
        total_mem_total="$((total_mem_total + mem_total))"
        total_mem_used="$((total_mem_used + mem_used))"
        total_mem_free="$((total_mem_free + mem_free))"
        total_mem_shared="$((total_mem_shared + mem_shared))"
        total_mem_buff="$((total_mem_buff + mem_buff))"
        total_mem_available="$((total_mem_available + mem_available))"
        total_swap_total="$((total_swap_total + swap_total))"
        total_swap_used="$((total_swap_used + swap_used))"
        total_swap_free="$((total_swap_free + swap_free))"
    done < "$minute_log_file"
done

# Diagnegregasikan path size
total_path_size=0
for path_size in "${path_sizes[@]}"; do
    # Remove the 'M' character and any leading/trailing whitespace
    path_size="${path_size//[^0-9]/}"
    
    # Make sure the path_size is not empty
    if [[ -n "$path_size" ]]; then
        total_path_size="$((total_path_size + path_size))"
        if ((path_size > max_path_size)); then
            max_path_size="$path_size"
        fi
        if ((path_size < min_path_size && path_size > 0)); then
            min_path_size="$path_size"
        fi
    fi
done

average_path_size="$((total_path_size / 3))M"

# Hitung rata-rata metrik
mem_total_avg="$((total_mem_total / 3))"
mem_used_avg="$((total_mem_used / 3))"
mem_free_avg="$((total_mem_free / 3))"
mem_shared_avg="$((total_mem_shared / 3))"
mem_buff_avg="$((total_mem_buff / 3))"
mem_available_avg="$((total_mem_available / 3))"
swap_total_avg="$((total_swap_total / 3))"
swap_used_avg="$((total_swap_used / 3))"
swap_free_avg="$((total_swap_free / 3))"

# Simpan hasil aggregasinya
echo "type, mem_total, mem_used, mem_free, mem_shared, mem_buff, mem_available, swap_total, swap_used, swap_free, path_size" > "$file"
echo "minimum:, $mem_total_min, $mem_used_min, $mem_free_min, $mem_shared_min, $mem_buff_min, $mem_available_min, $swap_total_min, $swap_used_min, $swap_free_min, ${min_path_size}M" >> "$file"
echo "maximum:, $mem_total_max, $mem_used_max, $mem_free_max, $mem_shared_max, $mem_buff_max, $mem_available_max, $swap_total_max, $swap_used_max, $swap_free_max, ${max_path_size}M" >> "$file"
echo "average:, $mem_total_avg, $mem_used_avg, $mem_free_avg, $mem_shared_avg, $mem_buff_avg, $mem_available_avg, $swap_total_avg, $swap_used_avg, $swap_free_avg, $average_path_size" >> "$file"
```
### Pembahasan Jawaban

##### mengambil ram_info dan disk_info
```
mengambil ram_info dan disk menggunakan
ram_info=$(free -m | grep -E 'Mem' | awk '{printf "%s, %s, %s, %s, %s, %s", $2, $3, $4, $5, $6, $7}')

swap_info=$(free -m | grep -E 'Swap' | awk '{printf "%s, %s, %s", $2, $3, $4}')

username=$(whoami)
target_path="/home/$username/"
directory_size=$(du -sh "$target_path" | cut -f1)
```
sesuai dengan permintaan soal kita menggunakan `du -sh` dan `free -m`.

##### a.
```
echo "mem_total, mem_used, mem_free, mem_shared, mem_buff, mem_available, swap_total, swap_used, swap_free, path, path_size" > "$log_directory/metrics_${timestamp}.log"
metrics="$ram_info, $swap_info, $target_path, $directory_size"
echo "$metrics" >> "$log_directory/metrics_${timestamp}.log"
```
Kita masukan semua info yang didapat tadi kedalam `metrics_{timestamp}.log`

##### b.
```
#b. cron every minute (min hour day bulan minggu)
# * * * * * ./minute_log.sh
```
pakai `cron` untuk mengotomasikan file per menitnya dimana setiap `*` pada `* * * * *` merepresentasikan menit jam hari bulan minggu. 

##### c.
```
# Cron per jam
# 0 * * * * ./aggregate_minutes_to_hourly_log.sh

(untuk aggregasi mohon lihat file dikarenakan panjang)

echo "type, mem_total, mem_used, mem_free, mem_shared, mem_buff, mem_available, swap_total, swap_used, swap_free, path_size" > "$file"
echo "minimum:, $mem_total_min, $mem_used_min, $mem_free_min, $mem_shared_min, $mem_buff_min, $mem_available_min, $swap_total_min, $swap_used_min, $swap_free_min, ${min_path_size}M" >> "$file"
echo "maximum:, $mem_total_max, $mem_used_max, $mem_free_max, $mem_shared_max, $mem_buff_max, $mem_available_max, $swap_total_max, $swap_used_max, $swap_free_max, ${max_path_size}M" >> "$file"
echo "average:, $mem_total_avg, $mem_used_avg, $mem_free_avg, $mem_shared_avg, $mem_buff_avg, $mem_available_avg, $swap_total_avg, $swap_used_avg, $swap_free_avg, $average_path_size" >> "$file"
```
otomasikan file per jam menggunakan cron `0 * * * *`, dan aggregasinya saya menggunakan `if` perbandingan, dan langsung dicetak per line nya

Masalah yang ditemukan pada soal ini terdapat pada kemungkinan pada besarnya file aggregasi jadinya terkadang saling bikin error dan typo dan bug mudah muncul. begitu saja. jika dilihat lagi sebenarnya logika yang dipakai sangatlah sederhana setelah kita selesai mengerjakan namun panjang dan banyak jadinya membuat banyak kemungkinan untuk terjadi kesalahan. dan pada saat ada banyak kesalahan dan saat kita baca kembali logikanya itu membuat kita tunnel visioning dan frustasi. yang akhirnya membuat lebih banyak masalah. dan tambahan saya juga mendapatkan masalah dengan aggregasi rata-rata, hal tersebut banyak sekali error nya dimana hasil yang dikeluarkan tidak masuk akal dimana nilainya lebih besar dari pada nilai maksimum.

### Output
![output metrics_{timestamp}](images/metrics_.log.png)
![output metrics_agg_{timestamp}](images/metrics_agg.log.png)

### Revisi

##### Menambahkan No. d (yan g saya lupa tambhakan) 
```
# d. Membuat semua .log hanya dapat dibaca oleh owner
chmod 600 $log_directory/*.log
```
mengubah .log menjadi hanya dapat dibaca oleh owner kita dapat menggunakan `chmod` dan `600` mengindikasikan bahwa `6--` adalah permission untuk owner dimana owner memiliki read `4` dan write `2` permission, dan `-00` dan kedua di belakang ini adalah indikasikan group dan others dimana mereka tidak memiliki permission apapun.

##### perbaiki file path dan file size (teryata hanya salah penempatan)
```
username=$(whoami)
target_path="/home/$username/"

  (..)

path_size="${path_size_column//[^0-9]/}"  # menghilankan semua karakter kecuali angka
  if [[ -n "$path_size" ]]; then
    path_size="$((path_size * 1))"  # ganti jadi angka valid
  else
    path_size=0
  fi

  (..)

  if ((path_size < min_path_size && path_size > 0)); then
            min_path_size="$path_size"
        fi
        if ((path_size > max_path_size)); then
        max_path_size="$path_size"
    	fi
```
saya aslinya sudah menaruh ini semua hanya saja penempatannya salah jadinya di aggregasi terlebih dahulu sebelum dipisah 'M' nya jadi terjadi bug.
